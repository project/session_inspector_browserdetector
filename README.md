# Session Inspector BrowserDetector

The Session Inspector BrowserDetector module formats the browser output of
the Session Inspector module using the PHP Browser Detection library.

This adds two formats to the module.
- BrowserDetector format detailed (eg. Chrome 119.0.0.0 - Linux)
- BrowserDetector format simple (eg. Chrome - Linux)

If any problems were encountered whilst running the browser detection library
then the plugins will return the original string. If this happens then make
sure you have all the composer packages installed correctly.

For a full description of the module visit
[project page](https://www.drupal.org/project/session_inspector_browserdetector)

To submit bug reports and feature suggestions, or to track changes visit
[issue queue](https://www.drupal.org/project/issues/session_inspector_browserdetector)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

Drupal core modules
- User

External projects
- [Session Inspector](https://www.drupal.org/project/session_inspector)
- [PHP Browser Detection](https://github.com/foroco/php-browser-detection)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Assuming you already have the session inspector module installed.

1. Enable the module at Administration > Extend.
2. Head to the Session Inspector Configuration page at the path
`/admin/config/people/session_inspector`.
3. Configure the "Browser format" to with a different format.

Note: The browser is only recorded with the session after the session inspector
module has been installed. If you haven't logged in since installing the module
then you'll see "unknown" for the browser information.

## Maintainers

- Phil Norton - [philipnorton42](https://www.drupal.org/u/philipnorton42)

Supporting organizations:

- [#! code (Hash Bang Code)](https://www.drupal.org/code-hash-bang-code)
