<?php

namespace Drupal\session_inspector_browserdetector\Plugin\BrowserFormat;

use Drupal\Core\Plugin\PluginBase;
use Drupal\session_inspector\Plugin\BrowserFormatInterface;
use foroco\BrowserDetection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides full browser detector format.
 *
 * @BrowserFormat(
 *   id = "browser_detector_detailed",
 *   name = @Translation("BrowserDetector format detailed")
 * )
 */
class BrowserDetectorBrowserFormatDetailed extends PluginBase implements BrowserFormatInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formatBrowser(string $browser):string {
    try {
      $browserDetection = new BrowserDetection();
      $result = $browserDetection->getAll($browser);
    }
    catch (\Throwable $e) {
      // If a problem happens with translating the browser then return the
      // original browser string.
      return $browser;
    }

    return $result['browser_name'] . ' ' . $result['browser_version'] . ' - ' . $result['os_name'] . ($result['os_version'] ? ' ' . $result['os_version'] : '');
  }

}
